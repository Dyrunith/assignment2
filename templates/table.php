<h1>College Address Book</h1>
<table id="myTable" class="table table-striped">
    <!-- DO NOT CHANGE ABOVE -->
    <thead>
    <tr>
        <th>#</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>City</th>
    </tr>
    </thead>
    <tbody>

    <?php
        foreach ($data as $output) {
            echo "<tr>";
                echo "<td>".$output['number']."</td>";
                echo "<td id='first_name:1' contenteditable='true'>".$output['firstname']."</td>";
                echo "<td id='last_name:1' contenteditable='true'>".$output['Lastname']."</td>";
                echo "<td id='city:1' contenteditable='true'>".$output['City']."</td>";
            echo "</tr>";
        }
    ?>

    </tbody>

</table>
<!-- DO NOT CHANGE BELOW -->
<button type="button" id="add" class="btn btn-default" aria-label="Left Align">
    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
</button> Add new Contact
